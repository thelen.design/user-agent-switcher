#!/bin/sh
DISTFILE="web-ext-artifacts/user-agent-switcher.xpi"

################################
# Build release extension file #
################################
TEMPFILE="$(web-ext build --overwrite-dest | tee /dev/stderr | grep "^Your web extension is ready:" | cut -d':' -f2- | cut -b2-)"
code=$?
if [ $code -ne 0 ];
then
	exit $code
fi
if [ -z "${TEMPFILE}" ];
then
	echo "Could not detect \`web-ext\` target file path" >&2
	exit 1
fi

############################
# Determine target version #
############################
version="$(grep --only-matching '"version":\s*".*"' "manifest.json" | cut -d'"' -f4)"
if [ -z "${version}" ];
then
	echo "Could not detect the current version number" >&2
	exit 1
fi

######################################
# Make sure version number is unique #
######################################
if git show-ref "v${version}" >/dev/null;
then
	echo "Release ${version} already exists, please increment the version number in the \"manifest.json\" file" >&2
	exit 2
fi

###########################################
# Make sure everything has been committed #
###########################################
if [ -n "$(git status --short)" ];
then
	echo "There are uncommitted files in your repository, please commit or stash them before continuing" >&2
	exit 3
fi

#################################
# Tag the newly created version #
#################################
git tag --sign -m "Version ${version}" "v${version}"

#########################
# Archive the final XPI #
#########################
DISTFILE_DIRNAME="$(dirname "${DISTFILE}")"
DISTFILE_SUFFIX="$(echo "${DISTFILE}" | grep -E --only-matching "[.][a-zA-Z0-9]+$")"
DISTFILE_BASENAME="$(basename "${DISTFILE}" "${DISTFILE_SUFFIX}")"
mv "${TEMPFILE}" "${DISTFILE_DIRNAME}/${DISTFILE_BASENAME}-${version}${DISTFILE_SUFFIX}"